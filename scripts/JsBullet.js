var JsBullet = function(path, enemy ) {
  // Sets the objects texture to the image parameter.
  PIXI.Sprite.call(this, PIXI.loader.resources["./resources/images/JsBullet.png"].texture);

  // Used to know what the last waypoint is.
  this.pathCycle = 0;

  // Set the objects x and y to the first position in the path.

  // Anchor will place the texture in the middle of x and y.
  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  // Sets the objects variables.
  this.damage = 5;
  this.enemy = enemy;
  this.rotation = 1;
  this.scale.x = 1.5
  this.scale.y = 1.5;
  this.baseSpeed = 5;
  this.speed = this.baseSpeed;
  this.path = path.slice();
  this.path.reverse();
  this.lifetime = 300;
  this.sold = false;
  this.amCoolDown = 0;

  this.x = this.path[0].x;
  this.y = this.path[0].y;
}

JsBullet.prototype = Object.create(PIXI.Sprite.prototype);

JsBullet.prototype.baseUpdate = function() {
  this.update();
  this.move(this.x, this.y, this.speed);
  this.collision();
};

JsBullet.prototype.update = function() {
  if (this.amCoolDown <= 0){
    this.scale.y *= -1;
    this.amCoolDown = 10;
  }
  this.amCoolDown--;
  // Called each time a frame is draw and is used in inherited classes.
};

JsBullet.prototype.collision = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.enemy[i].width) {
      this.enemy[i].hit(this.damage);
    }
  }
}

JsBullet.prototype.move = function(x, y, speed) {
  // Getting last and next waypoint positions.
  if (this.pathCycle + 1 < this.path.length) {
    var x1 = this.path[this.pathCycle].x;
    var y1 = this.path[this.pathCycle].y;
    var x2 = this.path[this.pathCycle + 1].x;
    var y2 = this.path[this.pathCycle + 1].y;

    // Getting the angle from the last waypoint to the next.
    var rotation = GameMath.twoPointRotation(x1, y1, x2, y2);

    // Getting the distance between the current x and y to the next waypoint.
    var distance = GameMath.distance(x, y, x2, y2)

    // Checks if the distance between current point is less than the speed.
    if (distance <= speed) {
      // Set the cycle to next index in the path.
      this.pathCycle++;

      // Call the function again using the x2 and y2 as the current pos and removes the distance from the speed.
      this.move(x2, y2, speed - distance);
    } else {
      // Moves the current pos using the rotation and speed.
      this.moveToTaget(x, y, rotation, speed);
    }
  } else {
    this.sold = true;
  }
};

JsBullet.prototype.moveToTaget = function(x, y, rotation, speed) {
  // Sets the objects x and y to the new coords.

  this.x = x + Math.cos(rotation) * speed;
  this.y = y + Math.sin(rotation) * speed;

  this.rotation = rotation;
};
