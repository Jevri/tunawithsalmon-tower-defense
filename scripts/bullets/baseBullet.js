var baseBullet = function(image, x, y, rotation, damage, speed, enemy) {
  // Sets the objects texture to the image parameter.
  PIXI.Sprite.call(this, image);
  // Set the objects x and y to the first position in the path.
  this.x = x;
  this.y = y;
  this.rotation = rotation;

  // Anchor will place the texture in the middle of x and y.
  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  // Sets the objects variables.
  this.damage = damage;
  this.speed = speed;
  this.enemy = enemy;
  this.lifetime = 300;
  this.dead = false;
}

baseBullet.prototype = Object.create(PIXI.Sprite.prototype);

baseBullet.prototype.baseUpdate = function() {
  this.update();
  this.move();
  this.collision();
  this.lifeTimeUpdate();
};

baseBullet.prototype.update = function() {
  // Called each time a frame is draw and is used in inherited classes.
};

baseBullet.prototype.lifeTimeUpdate = function() {
  if (this.lifetime <= 0) {
    this.dead = true;
  } else {
    this.lifetime--;
  }
};

baseBullet.prototype.move = function() {
  if (!this.dead) {
    this.x += Math.cos(this.rotation) * this.speed;
    this.y += Math.sin(this.rotation) * this.speed;
  }
}

baseBullet.prototype.collision = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.enemy[i].width) {
      this.enemy[i].hit(this.damage);
      this.dead = true;
      break;
    }
  }
}
