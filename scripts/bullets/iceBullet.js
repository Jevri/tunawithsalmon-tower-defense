var IceBullet = function(image, x, y, rotation, damage, speed, radius, enemy) {
  // Calls base usng the textures for the tower.
  baseBullet.call(this, image, x, y, rotation, damage, speed, enemy);

  // Sets tower variables.
  this.radius = radius;
}

IceBullet.prototype = Object.create(baseBullet.prototype);


// Calls freezeArea then hits an enemy
IceBullet.prototype.collision = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.enemy[i].width) {
      this.freezeArea();
      this.dead = true;
      break;
    }
  }
}
// Freezes all enemies withing the radius
IceBullet.prototype.freezeArea = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.radius) {
      this.enemy[i].Icehit();
      this.enemy[i].hit(this.damage);
    }
  }
}
