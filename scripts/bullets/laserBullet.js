var LaserBullet = function(image, x, y, rotation, damage, speed, enemy) {
  // Calls base usng the textures for the tower.
  baseBullet.call(this, image, x, y, rotation, damage, speed, enemy);

  // Sets tower variables.
}

LaserBullet.prototype = Object.create(baseBullet.prototype);


// Calls freezeArea then hits an enemy
LaserBullet.prototype.collision = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.enemy[i].width) {
      this.enemy[i].hit(this.damage)
      break;
    }
  }
}
