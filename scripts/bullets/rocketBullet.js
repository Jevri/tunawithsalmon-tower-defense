var RocketBullet = function(image, x, y, rotation, damage, speed, radius, enemy) {
  // Calls base usng the textures for the tower.
  baseBullet.call(this, image, x, y, rotation, damage, speed, enemy);

  // Sets tower variables.
  this.radius = radius;
}

RocketBullet.prototype = Object.create(baseBullet.prototype);

RocketBullet.prototype.update = function() {
  var target;
  var distance = 1000000;
  for (var i = 0; i < this.enemy.length; i++) {
    var currentDistance = GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y);
    if (currentDistance < distance) {
      target = i;
      distance = currentDistance;
    }
  }
  if (target != null) {
    this.rotation = GameMath.twoPointRotation(this.x, this.y, this.enemy[target].x, this.enemy[target].y);
  }
}

RocketBullet.prototype.collision = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.enemy[i].width) {
      this.blastDamage();
      this.dead = true;
      break;
    }
  }
}

RocketBullet.prototype.blastDamage = function() {
  for (var i = 0; i < this.enemy.length; i++) {
    if (GameMath.distance(this.x, this.y, this.enemy[i].x, this.enemy[i].y) < this.radius) {
      this.enemy[i].hit(this.damage);
    }
  }
}
