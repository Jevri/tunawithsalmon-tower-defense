var gameController = function(map, gameoverFunction) {
  this.container = new PIXI.Container();
  this.map = map;
  this.gameoverFunction = gameoverFunction;
  this.setup();
}

gameController.prototype.setup = function() {
  this.bullets = [];
  this.hud;

  this.setBackground();
  this.setScale();

  this.container.x = renderer.width / 2 - this.container.width / 2;
  this.container.y = 10;

  this.setupControllers();
  this.setupOverlay();
}

gameController.prototype.setupControllers = function() {
  this.player = new Player();
  this.waveControl = new WaveController(this.container, this.player, this.map);
  this.towerControl = new TowerController(this.container, this.waveControl.enemies, this.bullets, this.player, this.hud, this.background, this.map);
}

gameController.prototype.setupOverlay = function() {
  this.hud = new Hud(this.container, this.player, this.towerControl);
  this.nextWaveButton();
  this.cursor = new Cursor(PIXI.loader.resources["Cursor"].texture, this.towerControl.towers,this.map);
  this.container.addChild(this.cursor);

  this.towerControl.menu = this.hud;
}

gameController.prototype.setScale = function() {
  this.calculatedScale = (renderer.height - 20) / this.background.height;

  this.container.scale.x = this.calculatedScale;
  this.container.scale.y = this.calculatedScale;
}

gameController.prototype.setBackground = function() {
  this.background = new Sprite(resources[this.map.name].texture);
  this.backgroundEnableInteractive();
  this.container.addChild(this.background);
}

gameController.prototype.backgroundEnableInteractive = function() {
  this.background.interactive = true;
  this.background.on('pointerdown', (event) => {
    this.hud.openMenu(this.cursor.isHit, event.data.getLocalPosition(this.background));
  });
  this.background.on('pointermove', (event) => {
    this.cursor.baseUpdate(event.data.getLocalPosition(this.container));
  });
}

gameController.prototype.nextWaveButton = function() {
  this.nextButton = new NextButton(this.background.width, this.background.height);
  this.container.addChild(this.nextButton);
}

gameController.prototype.update = function() {
  this.towerControl.update();
  this.updateGameObjects();
  this.waveControl.update(this.nextButton.toggled);
  this.playerUpdate();
}

gameController.prototype.playerUpdate = function() {
  if (this.player.isDead()) {
    this.gameOver();
  }

  this.hud.update(this.waveControl.wave.waveNum, this.player.health, this.player.money);
}

gameController.prototype.updateGameObjects = function() {
  this.updateBullets();
}


gameController.prototype.updateBullets = function() {
  for (var i = 0; i < this.bullets.length; i++) {
    this.bullets[i].baseUpdate();

    if (this.bullets[i].dead) {
      GameMath.removeFromArray(this.container, this.bullets, i);
      i--;
    }
  }
}

gameController.prototype.gameOver = function() {
  this.gameoverFunction();
}
