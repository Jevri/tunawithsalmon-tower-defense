var TowerController = function (container, enemies, bullets, player, menu, background,map){
  this.towers = [];

  this.container = container;
  this.enemies = enemies;
  this.bullets = bullets;
  this.player = player;
  this.menu = menu;
  this.background = background;
  this.map = map;
  this.setup();
}

TowerController.prototype.setup = function() {}

TowerController.prototype.update = function() {
  this.updateTowers();
}

TowerController.prototype.updateTowers = function() {
  for (var i = 0; i < this.towers.length; i++) {
    if (!this.towers[i].sold) {
      this.towers[i].baseUpdate();
    } else {
      GameMath.removeFromArray(this.container, this.towers, i);
      i--;
    }
  }
}

TowerController.prototype.placeTower= function(x, y,tower){
  if (tower != JsBullet){
    var tower = new tower(x, y, this.enemies, this.bullets, this.container, this.player, this.menu, this.background);
  } else {
    var tower = new tower(this.map.waypoints,this.enemies);
  }
  this.towers.push(tower);
  this.container.addChildAt(tower, 2);
}
