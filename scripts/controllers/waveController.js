var WaveController = function(container, player, map) {
  this.container = container;
  this.player = player;
  this.map = map;

  this.wave = new Wave();
  this.enemies = [];

  this.spawnTimer = 0;
  this.waveTime = 20;
}

WaveController.prototype.update = function(toggle) {
  this.updateEnemies();
  this.spawnEnemy(toggle);
  this.spawnTimer--;
};

WaveController.prototype.spawnEnemy = function(toggle) {
  if (this.wave.waveList.length > 0) {
    this.addToEnemies();
  } else if ((this.enemies.length <= 0 || toggle) && this.wave.waveList.length <= 0) {
    this.wave.createWave();
    this.player.money += 200;
    this.waveTime = 20 - this.wave.waveNum;
  }
}

WaveController.prototype.addToEnemies = function() {
  if (this.spawnTimer <= 0) {
    var choice = this.wave.getEnemy();
    this.enemies.push(new choice(10 + 25 * this.wave.waveNum, 2 * this.wave.waveNum, 1 + this.wave.waveNum * 0.15, this.map.waypoints));
    this.container.addChild(this.enemies[this.enemies.length - 1]);
    this.spawnTimer = this.waveTime;
  }
}

WaveController.prototype.updateEnemies = function() {
  for (var i = 0; i < this.enemies.length; i++) {
    this.enemies[i].baseUpdate();

    if (this.enemies[i].dead) {
      this.player.money += this.enemies[i].moneyDrop;
      GameMath.removeFromArray(this.container, this.enemies, i);
      i--;
    } else if (this.enemies[i].done) {
      GameMath.removeFromArray(this.container, this.enemies, i);
      this.player.health--;
      i--;
    }
  }
}
