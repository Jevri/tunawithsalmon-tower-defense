var Cursor = function(image, towers, map){
  PIXI.Sprite.call(this, image);
  this.towers = towers;
  this.map = map
  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  this.isHit = false;
}

Cursor.prototype = Object.create(PIXI.Sprite.prototype);

Cursor.prototype.baseUpdate = function (pos) {
  this.x = pos.x;
  this.y = pos.y;

  this.isHit = false;
  for (var i = 0; i < this.towers.length; i++) {
    if (GameMath.distance(this.x, this.y, this.towers[i].x, this.towers[i].y) < this.towers[i].width){
      this.isHit = true;
    }
  }

  x = Math.floor(this.x / this.map.gridSize);
  y = Math.floor(this.y / this.map.gridSize);

  if (this.map.grid[x + y * this.map.tileAmount] == 1){
    this.isHit = true;
  }



  if (!this.isHit) this.tint = 0x00ff00;
  else this.tint = 0xff0000;

};
