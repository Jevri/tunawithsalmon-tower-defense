var BaseEnemy = function(image, health, moneyDrop, speed, path) {
  // Sets the objects texture to the image parameter.
  PIXI.Sprite.call(this, image);
  this.x = path[0].x;
  this.y = path[0].y;

  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  this.health = health;
  this.moneyDrop = moneyDrop;
  this.baseSpeed = speed;
  this.path = path;

  this.pathCycle = 0;
  this.freezeTimer = 0;
  this.dead = false;
  this.done = false;
  this.speed = this.baseSpeed;
}

BaseEnemy.prototype = Object.create(PIXI.Sprite.prototype);

BaseEnemy.prototype.baseUpdate = function() {
  this.update();
  this.freezed();
  this.move(this.x, this.y, this.speed);
};

BaseEnemy.prototype.update = function() {
  // Called each time a frame is draw and is used in inherited classes.
};

BaseEnemy.prototype.move = function(x, y, speed) {
  if (!this.isDone()) {
    this.getNextPos(x,y,speed);
  }
};

BaseEnemy.prototype.isDone = function(x, y, rotation, speed) {
  if(this.pathCycle + 1 >= this.path.length){
    this.done = true;
    return true;
  }
  return false;
};

BaseEnemy.prototype.getNextPos = function(x, y, speed) {
  var x1 = this.path[this.pathCycle].x;
  var y1 = this.path[this.pathCycle].y;
  var x2 = this.path[this.pathCycle + 1].x;
  var y2 = this.path[this.pathCycle + 1].y;

  var rotation = GameMath.twoPointRotation(x1, y1, x2, y2);
  var distance = GameMath.distance(x, y, x2, y2)

  if (distance <= speed) {
    this.pathCycle++;
    this.move(x2, y2, speed - distance);
  } else {
    this.moveToTaget(x, y, rotation, speed);
  }
};

BaseEnemy.prototype.moveToTaget = function(x, y, rotation, speed) {
  this.x = x + Math.cos(rotation) * speed;
  this.y = y + Math.sin(rotation) * speed;

  this.rotation = rotation;
};

BaseEnemy.prototype.hit = function(damage) {
  this.health -= damage;
  if (this.health <= 0) {
    this.dead = true;
  }
};

BaseEnemy.prototype.freezed = function() {
  if (this.freezeTimer >= 0) {
    this.freezeTimer--;
  } else this.speed = this.baseSpeed;
}

BaseEnemy.prototype.Icehit = function() {
  this.freezeTimer = 120;
  this.speed = this.baseSpeed / 4;
};

BaseEnemy.prototype.getDistanceTravel = function() {
  var distance = 0;

  for (var i = 0; i < this.pathCycle; i++) {
    distance += GameMath.distance(this.path[i].x, this.path[i].y, this.path[i + 1].x, this.path[i + 1].y)
  }
  distance += GameMath.distance(this.path[i].x, this.path[i].y, this.x, this.y);
  return distance;
};
