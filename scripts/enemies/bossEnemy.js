var BossEnemy = function(health, moneyDrop, speed, path) {
  // Sets the objects texture to the image parameter.
  BaseEnemy.call(this, PIXI.loader.resources["./resources/images/IceBullet.png"].texture, health, moneyDrop, speed, path);

  this.health = health * 3;
  this.moneyDrop = moneyDrop * 2;
}
BossEnemy.prototype = Object.create(BaseEnemy.prototype)

// Freezes enemy then hit for 2 sec.
BossEnemy.prototype.Icehit = function() {
  this.freezeTimer = 120;
  this.speed = this.baseSpeed / 2;
};
