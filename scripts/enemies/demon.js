var Demon = function(health, moneyDrop, speed, path) {
  this.imageOpenMouth = PIXI.loader.resources["./resources/images/DemonOpenMouth.png"].texture;
  this.imageDemon = PIXI.loader.resources["./resources/images/Demon.png"].texture;
  // Sets the objects texture to the image parameter.
  BaseEnemy.call(this, this.imageDemon, health, moneyDrop, speed, path);

  this.amCoolDown = 0;
  this.bol = false;
}

Demon.prototype = Object.create(BaseEnemy.prototype);

Demon.prototype.update = function() {
  // Called each time a frame is draw and is used in inherited classes.
  if (this.amCoolDown == 0) {
    this.bol = !this.bol;
    if (this.bol) {
      this.texture = this.imageDemon;
    } else {
      this.texture = this.imageOpenMouth;
    }
    this.amCoolDown = 20;
  }
  this.amCoolDown--;

}
Demon.prototype.moveToTaget = function(x, y, rotation, speed) {
  // Sets the objects x and y to the new coords.

  this.x = x + Math.cos(rotation) * speed;
  this.y = y + Math.sin(rotation) * speed;

};
