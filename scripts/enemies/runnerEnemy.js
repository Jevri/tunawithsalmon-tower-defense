var RunnerEnemy = function(health, moneyDrop, speed, path) {
  // Sets the objects texture to the image parameter.
  BaseEnemy.call(this, PIXI.loader.resources["./resources/images/runner.png"].texture, health, moneyDrop, speed, path);

  this.health = health / 2;
  this.basespeed *= 2;
}
RunnerEnemy.prototype = Object.create(BaseEnemy.prototype)
