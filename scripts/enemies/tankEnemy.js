var TankEnemy = function(health, moneyDrop, speed, path) {
  // Sets the objects texture to the image parameter.
  BaseEnemy.call(this, PIXI.loader.resources["./resources/images/TankEnemy.png"].texture, health, moneyDrop, speed, path);

  this.health = health * 10;
  this.baseSpeed /= 2;
}
TankEnemy.prototype = Object.create(BaseEnemy.prototype)

TankEnemy.prototype.Icehit = function() {
  this.freezeTimer = 120;
  this.speed = this.baseSpeed / 2;
};
