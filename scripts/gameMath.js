function GameMath() {}

GameMath.twoPointRotation = function(x1, y1, x2, y2) {
  return Math.atan2(y2 - y1, x2 - x1);
}

GameMath.distance = function(x1, y1, x2, y2) {
  var a = x1 - x2;
  var b = y1 - y2;

  return Math.sqrt(a * a + b * b);
}

GameMath.removeFromArray = function(container, array, index) {
  container.removeChild(array[index]);
  array.splice(index, 1);
}
