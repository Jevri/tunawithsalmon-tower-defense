var Container = PIXI.Container,
    autoDetectRenderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite;

var stage = new Container(),
    gameoverContainer = new Container(),
    menu = new Container(),
    renderer = autoDetectRenderer(256, 256);

renderer.backgroundColor = 5258522;
renderer.view.style.position = "absolute";
renderer.view.style.display = "block";
renderer.autoResize = true;
renderer.resize(window.innerWidth, window.innerHeight);

document.body.appendChild(renderer.view);

var state = dummy;
var controller;


loader
  .add("./resources/images/IceBullet.png")
  .add("./resources/images/runner.png")
  .add("./resources/images/IceTower.png")
  .add("SimpleMap", "./resources/maps/simpleMap.png")
  .add("DemonMap", "./resources/maps/demonlevel.png")
  .add("./resources/images/JsBullet.png")
  .add("./resources/images/Demon.png")
  .add("./resources/images/DemonOpenMouth.png")
  .add("./resources/images/RocketBullet.png")
  .add("./resources/images/RocketTower.png")
  .add("./resources/images/TankEnemy.png")
  .add("./resources/images/CarTower.png")
  .add("./resources/images/ShotgunTower.png")
  .add("./resources/images/ArmyTurret.png")
  .add("./resources/images/GatlingBullet.png")
  .add("./resources/images/LaserBullet.png")
  .add("./resources/images/GatlingTower.png")
  .add("./resources/images/Enemy.png")
  .add("./resources/images/Bullet.png")
  .add("./resources/images/ArmyBullet.png")
  .add("Cursor", "./resources/images/cursor.png")
  .add("./resources/images/button.png")
  .load(setup);

var controller;
var renderContainer = menu;

function setup() {
  gameov = new GameoverMenu(() => {
    this.renderContainer = menu;
    while (stage.children[0]) { stage.removeChild(stage.children[0]); }
  });
  gameoverContainer.addChild(gameov.container);

  mainMenu = new MainMenu(maps, (map) => {
    controller = new gameController(map, () => {
      this.state = dummy;
      this.renderContainer = gameoverContainer;
    });
    stage.addChild(controller.container);
    renderContainer = stage;

    state = simolation;
    console.log("new maps Yay");
  });

  menu.addChild(mainMenu.container);
  gameLoop();
}

function gameLoop(){
  requestAnimationFrame(gameLoop);
  state();
  renderer.render(renderContainer);
}

function simolation(){
  controller.update();
}

function dummy(){
}
