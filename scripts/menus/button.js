var Button = function(x, y, cost, player, text) {
  PIXI.Sprite.call(this, PIXI.loader.resources["./resources/images/button.png"].texture);

  this.cost = cost;
  this.player = player;
  this.x = x;
  this.y = y;
  this.text = text;
  this.textAmountBox = this.addText(text, -this.width / 2 + 2,-5,this);
  this.textCostBox = this.addText("cost", -this.width / 2 + 2,5,this);

  this.buyFunction;

  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  this.scale.x = 1.5;
  this.scale.y = 1.5;

  this.interactive = true;
  this.on('pointerdown', () => {
      this.buyFunction();
    });
}

Button.prototype = Object.create(PIXI.Sprite.prototype);

Button.prototype.addBuy = function(buyFunction){
  this.buyFunction = buyFunction;
}

Button.prototype.addText = function (text,x, y, contain) {
  var basicText = new PIXI.Text(text);
  basicText.x = x;
  basicText.y = y;

  basicText.anchor.y = 0.5;

  basicText.height = 11;
  basicText.scale.x = basicText.scale.y;

  contain.addChild(basicText);
  return basicText;
};

Button.prototype.setText = function (amount , costText) {
  this.textAmountBox.text = this.text + ": " + amount.toFixed(1);
  this.textCostBox.text = "Cost: " + costText.toFixed(0);
};
