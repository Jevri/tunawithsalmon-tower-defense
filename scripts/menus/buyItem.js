var buyItem = function(image, x, y, cost, towerBuy, player, towercontroller, pos) {
  PIXI.Sprite.call(this, image);

  this.cost = cost;
  this.player = player;
  this.x = x;
  this.y = y;
  this.anchor.x = 0.5;
  this.anchor.y = 0.5;
  this.tower = towerBuy;
  this.towerControl = towercontroller;
  this.interactive = true;
  this.on('pointerdown', () => {
    if (this.player.money >= this.cost && this.canPlace(pos)) {
      this.towerControl.placeTower(pos.x, pos.y, this.tower);
      this.player.money -= this.cost;
    }
  });

  this.on('pointerover', () => {
  if (this.tower != JsBullet){
    this.t = new this.tower(pos.x, pos.y ,this.towerControl.enemies, this.towerControl.bullets, this.towerControl.container, this.towerControl.gameControl);
    this.t.circleRange.visible = true;
    this.t.showRange();
    this.towerControl.container.addChildAt(this.t, 3);
  }
  });

  this.on('pointerout', () => {
    this.towerControl.container.removeChild(this.t);
  });
}

buyItem.prototype = Object.create(PIXI.Sprite.prototype);

buyItem.prototype.canPlace = function(pos) {
  for (var i = 0; i < this.towerControl.towers.length; i++) {
    if (GameMath.distance(pos.x, pos.y, this.towerControl.towers[i].x, this.towerControl.towers[i].y) < this.towerControl.towers[i].width) {
      return false;
    }
  }

  return true;
}
