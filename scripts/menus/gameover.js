var GameoverMenu = function (done) {
    this.container = new PIXI.Container();
    this.done = done;
    this.setup();
}

GameoverMenu.prototype.setup = function () {
  this.mapText(renderer.width / 2, renderer.height / 2, 3);
};

GameoverMenu.prototype.mapText = function (x ,y, size) {
  this.gameoverText = new PIXI.Text("GameoverMenu");
  this.gameoverText.scale.x = size;
  this.gameoverText.scale.y = size;
  this.gameoverText.anchor.x = 0.5;
  this.gameoverText.anchor.y = 0.5;
  this.gameoverText.x = x;
  this.gameoverText.y = y;

  this.gameoverText.interactive = true;
  this.gameoverText.on("pointerdown", () =>{
    this.done();
    console.log("Hey")
  })
  this.container.addChild(this.gameoverText);
};
