var Hud = function(contain, player, towerControl) {
  this.container = contain;
  this.menu;
  this.player = player;
  this.towerControl = towerControl;

  this.waveText = this.addText("Wave: 0", 0 ,0, contain);
  this.liveText = this.addText("Lives: 0", 0, 30, contain);
  this.moneyText = this.addText("Money: 0", 0 , 60, contain);
  this.fps = this.addText("0", 0, 90, contain);
  this.fpsCounter = 0;
  this.date;
  this.d;
  this.od = 0;
}

Hud.prototype.addText = function (text,x, y, contain) {
  var basicText = new PIXI.Text(text);
  basicText.x = x;
  basicText.y = y;

  contain.addChild(basicText);
  return basicText;
};

Hud.prototype.update = function (waves, lives, money) {
  this.waveText.text = "Wave: " + waves;
  this.liveText.text = "Lives: " + lives;
  this.moneyText.text = "Money: " + money.toFixed(0);
  this.d = new Date().getTime();
  if (this.d >= this.od+1000){
    this.od = this.d;
    this.fps.text = "FPS: " + this.fpsCounter;
    this.fpsCounter = 0;
  }
  this.fpsCounter++;
};

Hud.prototype.openMenu = function (isHit, pos) {
  if (this.menu != null){
    this.container.removeChild(this.menu.getContainer());
    this.menu = null;
  }
  else if (!isHit){
    this.menu = new popupBuymenu(this.player, this.towerControl, pos);
    this.container.addChild(this.menu.getContainer());
  }
};
