var MainMenu = function (mapList, selectMapFunction) {
    this.container = new PIXI.Container();
    this.maps = mapList;
    this.selectMapFunction = selectMapFunction;
    this.selected = 0;
    this.setup();
}

MainMenu.prototype.setup = function () {
  this.mapImage();
  this.mapText(this.mapDisplay.x, this.mapDisplay.y - this.mapDisplay.height / 2 - 40, 2);
  this.placeButton();
};

MainMenu.prototype.mapText = function (x ,y, size) {
  this.mapHeader = new PIXI.Text("SimpleMap");
  this.mapHeader.scale.x = size;
  this.mapHeader.scale.y = size;
  this.mapHeader.anchor.x = 0.5;
  this.mapHeader.anchor.y = 0.5;
  this.mapHeader.x = x;
  this.mapHeader.y = y;

  this.container.addChild(this.mapHeader);
};

MainMenu.prototype.placeButton = function () {
  var firstStart = {x: this.mapDisplay.x + this.mapDisplay.width / 2 + 10, y: renderer.height / 2}
  var secondStart = {x: this.mapDisplay.x - this.mapDisplay.width / 2 - 10, y: renderer.height / 2}

  var width = 20;
  var height = 20;

  this.nextButton = this.newNextButton(firstStart.x, firstStart.y - height, firstStart.x + width, firstStart.y, firstStart.x, firstStart.y + height, 1)
  this.nextButton = this.newNextButton(secondStart.x, secondStart.y - height, secondStart.x - width, secondStart.y, secondStart.x, secondStart.y + height, -1)
};

MainMenu.prototype.mapImage = function () {
  this.mapDisplay = new PIXI.Sprite(PIXI.loader.resources[this.getSelected().name].texture);
  this.mapDisplay.x = renderer.width / 2;
  this.mapDisplay.y = renderer.height / 2;
  this.mapDisplay.scale.x = 0.4;
  this.mapDisplay.scale.y = 0.4;
  this.mapDisplay.anchor.x = 0.5;
  this.mapDisplay.anchor.y = 0.5;
  this.mapDisplay.interactive = true;
  this.mapDisplay.on("pointerdown", () => {
    this.selectMapFunction(this.getSelected());
  })
  this.container.addChild(this.mapDisplay);
};

MainMenu.prototype.newNextButton = function (x1,y1, x2,y2, x3,y3, clickValue) {
  var but = new PIXI.Graphics();
  but.beginFill(0x4e4e4f);
  but.moveTo(x1,y1);
  but.lineTo(x2,y2);
  but.lineTo(x3,y3);
  but.lineTo(x1,y1);
  but.endFill();

  but.interactive = true;
  but.on("pointerdown", () =>{
    this.selected += clickValue;
    this.mapDisplay.texture = PIXI.loader.resources[this.getSelected().name].texture;
    this.mapHeader.text = this.getSelected().name;
  });

  this.container.addChild(but);
  return but;
};

MainMenu.prototype.getSelected = function () {
  return (this.maps[Math.abs(this.selected % this.maps.length)]);
};
