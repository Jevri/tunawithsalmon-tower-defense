var NextButton = function(x, y) {
  PIXI.Sprite.call(this, PIXI.loader.resources["./resources/images/button.png"].texture);

  this.x = x - this.width / 2 - 5;
  this.y = y - this.height / 2 - 5;

  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  this.tint = 0x00ff00;

  this.txt = new PIXI.Text("Toggle");
  this.txt.anchor.x = 0.5;
  this.txt.anchor.y = 0.5;
  this.txt.height = 20;
  this.txt.scale.x = this.txt.scale.y;
  this.addChild(this.txt);

  this.interactive = true;
  this.on('pointerdown', (event) => {
    this.toggled = !this.toggled;

    if (this.toggled) this.tint = 0xff0000;
    else this.tint = 0x00ff00;
  });
}

NextButton.prototype = Object.create(PIXI.Sprite.prototype);
