var popupBuymenu = function (player, gameControl, pos) {
  this.pos = pos;
  this.buyMenu = new PIXI.Container();

  this.background = new PIXI.Graphics();
  this.background.beginFill(0x4e4e4f);
  this.background.moveTo(pos.x,pos.y);
  this.background.lineTo(pos.x + 10,pos.y + 10);
  this.background.lineTo(pos.x - 10,pos.y + 10);
  this.background.lineTo(pos.x,pos.y);
  this.background.endFill();

  this.background.beginFill(0xa8a8a8);
  this.background.drawRect(pos.x - 175, pos.y + 10, 350, 40);
  this.background.endFill();

  this.buyMenu.addChild(this.background);

  this.rocketTowerBuy = new buyItem(PIXI.loader.resources["./resources/images/RocketTower.png"].texture, pos.x - 50, pos.y + 30, 5000, RocketTower, player, gameControl, pos);
  this.buyMenu.addChild(this.rocketTowerBuy);

  this.armyTowerBuy = new buyItem(PIXI.loader.resources["./resources/images/ArmyTurret.png"].texture, pos.x, pos.y + 30, 250, ArmyTower, player, gameControl, pos);
  this.buyMenu.addChild(this.armyTowerBuy);

  this.iceTowerBuy = new buyItem(PIXI.loader.resources["./resources/images/IceTower.png"].texture, pos.x + 50, pos.y + 30, 1000, IceTower, player, gameControl, pos);
  this.buyMenu.addChild(this.iceTowerBuy);

  this.gatlingTowerBuy = new buyItem(PIXI.loader.resources["./resources/images/GatlingTower.png"].texture, pos.x-100, pos.y + 30, 100000, GatlingTower, player, gameControl, pos);
  this.gatlingTowerBuy.scale.x = 0.5;
  this.gatlingTowerBuy.scale.y = 0.5;
  this.buyMenu.addChild(this.gatlingTowerBuy);

  this.shotgunTowerBuy = new buyItem(PIXI.loader.resources["./resources/images/ShotgunTower.png"].texture, pos.x + 100, pos.y + 30, 500, ShotgunTower, player, gameControl, pos);
  this.buyMenu.addChild(this.shotgunTowerBuy);

  this.carTowerBuy = new buyItem(PIXI.loader.resources["./resources/images/CarTower.png"].texture, pos.x + 150, pos.y + 30, 10000, CarTower, player, gameControl, pos);
  this.carTowerBuy.scale.x = 0.5;
  this.carTowerBuy.scale.y = 0.5;
  this.buyMenu.addChild(this.carTowerBuy);

  this.jsBuy = new buyItem(PIXI.loader.resources["./resources/images/JsBullet.png"].texture, pos.x - 150, pos.y + 30, 100, JsBullet, player, gameControl, pos);
  this.buyMenu.addChild(this.jsBuy);

}

popupBuymenu.prototype.getContainer = function () {
  return this.buyMenu;
};
