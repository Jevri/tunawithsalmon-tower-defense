var PopupUpgradeMenu = function (player, tower, pos) {
  this.player = player;
  this.tower = tower;
  this.pos = pos;
  this.zOrder = 10;

  this.upgradeMenu = new PIXI.Container();

  this.upgradeMenu.interactive = true;
  this.upgradeMenu.on('pointerover', () => {
    this.tower.showRange();
    this.tower.circleRange.visible = true;
  });

  this.upgradeMenu.on('pointerout', () => {
    this.tower.circleRange.visible = false;;
  });
}

PopupUpgradeMenu.prototype.createBackground = function (x, y, width, height) {
  this.background = new PIXI.Graphics();
  this.background.beginFill(0x4e4e4f);
  this.background.moveTo(this.pos.x,this.pos.y);
  this.background.lineTo(this.pos.x + 10,this.pos.y + 10);
  this.background.lineTo(this.pos.x - 10,this.pos.y + 10);
  this.background.lineTo(this.pos.x,this.pos.y);
  this.background.endFill();

  this.background.beginFill(0xa8a8a8);
  this.background.drawRect(x, y, width, height);
  this.background.endFill();

  this.upgradeMenu.addChild(this.background);
};

PopupUpgradeMenu.prototype.addButton = function (x,y, upgardeValue, costTimes, propertyName, propertyCost, textMsg) {
  var but = new Button(x,y, this.player, this.tower, textMsg);
  but.setText(this.tower[propertyName], this.tower[propertyCost]);
  but.addBuy(() => {
    if (this.canAffort(this.tower[propertyCost])){
      this.tower[propertyName] += upgardeValue;
      this.tower[propertyCost] *= costTimes;
      but.setText(this.tower[propertyName], this.tower[propertyCost]);
    }
  });
  this.upgradeMenu.addChild(but);
};

PopupUpgradeMenu.prototype.canAffort = function (cost) {
  if (this.player.money >= cost){
    this.player.money -= cost;
    return true;
  }

  return false;
};

PopupUpgradeMenu.prototype.getContainer = function () {
  return this.upgradeMenu;
};
