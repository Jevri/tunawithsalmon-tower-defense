var Player = function() {
  this.health = 100;
  this.money = 500;
}

Player.prototype.isDead = function() {
  if (this.health <= 0) {
    return true;
  }

  return false;
};
