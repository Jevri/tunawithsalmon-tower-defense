var ArmyTower = function(x, y, enemies, bulletList, stg, player, menu, background) {
  // Calls base usng the textures for the tower.
  BaseTower.call(this, PIXI.loader.resources["./resources/images/ArmyTurret.png"].texture, PIXI.loader.resources["./resources/images/ArmyBullet.png"].texture, x, y, enemies, bulletList, stg, player, menu, background);

  // Sets tower variables.
  this.damage = 30;
  this.range = 200;
  this.cooldown = 20;

  this.damageCost = 50;
  this.rangeCost = 50;
  this.cooldownCost = 50;
}

ArmyTower.prototype = Object.create(BaseTower.prototype);

//Overriding parent function "shoow" to shoot two bullets.
ArmyTower.prototype.shoot = function() {
  b = new baseBullet(this.bullet, this.x + Math.cos(this.rotation + (Math.PI * 0.5)) * 10, this.y + Math.sin(this.rotation + (Math.PI * 0.5)) * 10, this.rotation, this.damage, 20, this.enemies);
  this.bulletList.push(b);
  this.stage.addChild(b);
  b = new baseBullet(this.bullet, this.x + Math.cos(this.rotation - (Math.PI * 0.5)) * 10, this.y + Math.sin(this.rotation - (Math.PI * 0.5)) * 10, this.rotation, this.damage, 20, this.enemies);
  this.bulletList.push(b);
  this.stage.addChild(b);
}

ArmyTower.prototype.openMenu = function(pos) {
  this.hud.menu = new PopupUpgradeMenu(this.player, this, pos);
  this.hud.menu.createBackground(pos.x - 155, pos.y + 10, 310, 60);
  this.hud.menu.addButton(pos.x - 100, pos.y + 40, 10, 1.5, "damage", "damageCost", "Damage");
  this.hud.menu.addButton(pos.x, pos.y + 40, 10, 1.5, "range", "rangeCost", "Range");
  this.hud.menu.addButton(pos.x + 100, pos.y + 40, -1, 1.5, "cooldown", "cooldownCost", "Firerate");
  this.stage.addChild(this.hud.menu.getContainer());
}
