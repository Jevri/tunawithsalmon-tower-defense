var BaseTower = function(image, bullet, x, y, enemies, bulletList, stg, player, hud, background) {
  PIXI.Sprite.call(this, image);

  this.anchor.x = 0.5;
  this.anchor.y = 0.5;

  this.x = x;
  this.y = y;
  this.enemies = enemies;
  this.bullet = bullet;
  this.bulletList = bulletList;
  this.stage = stg;
  this.player = player;
  this.hud = hud;
  this.background = background;

  this.damage;
  this.range;
  this.cooldown;
  this.cooldownTimer = 0;
  this.sold = false;

  this.damageCost = 100;
  this.rangeCost = 100;
  this.cooldownCost = 100;

  this.setInteractive();
}

BaseTower.prototype = Object.create(PIXI.Sprite.prototype);

BaseTower.prototype.setInteractive = function() {
  this.circleRange = new PIXI.Graphics();
  this.addChild(this.circleRange);

  this.circleRange.visible = false;

  this.interactive = true;
  this.on('pointerdown', (event) => {
    if (this.hud.menu != null){
      this.stage.removeChild(this.hud.menu.getContainer());
      this.hud.menu = null;
    }

    this.openMenu(event.data.getLocalPosition(this.background));
  });
}

BaseTower.prototype.baseUpdate = function() {
  this.update();
  this.checkRadius();
  this.showRange();
  this.cooldownTimer--;
};

BaseTower.prototype.update = function() {
  // Called each time a frame is draw and is used in inherited classes.
};

BaseTower.prototype.checkRadius = function() {
  var target;
  var longestWalk = 0;

  for (var i = 0; i < this.enemies.length; i++) {
    var currentDistance = GameMath.distance(this.x, this.y, this.enemies[i].x, this.enemies[i].y);
    var currentWalk = this.enemies[i].getDistanceTravel();
    if (currentDistance < this.range && currentWalk > longestWalk){
      target = i;
      longestWalk = currentWalk;
    }
  }

  if (target != null) {
    this.rotation = GameMath.twoPointRotation(this.x, this.y, this.enemies[target].x, this.enemies[target].y);
    this.tryShoot();
  }
}

BaseTower.prototype.tryShoot = function() {
  if (this.cooldownTimer <= 0) {
    this.shoot();
    this.cooldownTimer = this.cooldown;
  }
}

BaseTower.prototype.shoot = function() {
  b = new baseBullet(this.bullet, this.x, this.y, this.rotation, this.damage, 20, this.enemies);
  this.bulletList.push(b);
  this.stage.addChild(b);
}

BaseTower.prototype.openMenu = function(pos) {
  this.hud.menu = new PopupUpgradeMenu(this.player, this, pos);
  this.hud.menu.createBackground(pos.x - 155, pos.y + 10, 310, 60);
  this.hud.menu.addButton(pos.x - 100, pos.y + 40, 10, 1.5, "damage", "damageCost", "Damage");
  this.hud.menu.addButton(pos.x, pos.y + 40, 10, 1.5, "range", "rangeCost", "Range");
  this.hud.menu.addButton(pos.x + 100, pos.y + 40, -5, 1.5, "cooldown", "cooldownCost", "Firerate");
  this.stage.addChild(this.hud.menu.getContainer());
}

BaseTower.prototype.showRange = function() {
  this.circleRange.clear();
  this.circleRange.beginFill(0xFFFFFF, 0.25);
  this.circleRange.drawCircle(0, 0, this.range);
  this.circleRange.endFill();
}
