var CarTower = function(x, y, enemies, bulletList, stg, player, menu, background) {
  // Calls base usng the textures for the tower.
  BaseTower.call(
    this,
    PIXI.loader.resources["./resources/images/CarTower.png"].texture,
    PIXI.loader.resources["./resources/images/ArmyBullet.png"].texture,
    x, y, enemies, bulletList, stg, player, menu, background);

  // Sets tower variables.
  this.damage = 30;
  this.range = 500;
  this.cooldown = 20;
  this.speed = 1;
  this.noTagetRotation = (Math.random() -0.5) * 0.3;
  this.startX = x;
  this.startY = y;
  this.terrtory = 100;

  this.damageCost = 100;
  this.rangeCost = 100;
  this.cooldownCost = 100;
  this.speedCost = 20;
  this.terrtoryCost = 100;
}

CarTower.prototype = Object.create(BaseTower.prototype);

CarTower.prototype.update = function() {
  if (GameMath.distance(this.x,this.y,this.startX,this.startY) > this.terrtory){
    this.rotation = GameMath.twoPointRotation(this.x, this.y, this.startX, this.startY);
  }
    this.drive();
};

CarTower.prototype.drive = function(){
  this.x += Math.cos(this.rotation) * this.speed;
  this.y += Math.sin(this.rotation) * this.speed;
};

CarTower.prototype.openMenu = function(pos) {
  this.hud.menu = new PopupUpgradeMenu(this.player, this, pos);
  this.hud.menu.createBackground(pos.x - 255, pos.y + 10, 510, 60);
  this.hud.menu.addButton(pos.x - 100, pos.y + 40, 10, 1.5, "damage", "damageCost", "Damage");
  this.hud.menu.addButton(pos.x, pos.y + 40, 10, 1.5, "range", "rangeCost", "Range");
  this.hud.menu.addButton(pos.x + 100, pos.y + 40, -1, 1.5, "cooldown", "cooldownCost", "Firerate");
  this.hud.menu.addButton(pos.x - 200, pos.y + 40, 1, 1.5, "speed", "speedCost", "Speed");
  this.hud.menu.addButton(pos.x + 200, pos.y + 40, 50, 1.5, "terrtory", "terrtoryCost", "Distance");
  this.stage.addChild(this.hud.menu.getContainer());
}
