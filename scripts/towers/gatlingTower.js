var GatlingTower = function(x, y, enemies, bulletList, stg, player, menu, background){
  // Calls base usng the textures for the tower.
  BaseTower.call(this, PIXI.loader.resources["./resources/images/GatlingTower.png"].texture, PIXI.loader.resources["./resources/images/LaserBullet.png"].texture, x, y, enemies, bulletList, stg, player, menu, background);

  // Sets tower variables.
  this.damage = 50;
  this.range = 500;
  this.cooldown = 20;

  this.damageCost = 200;
  this.rangeCost = 200;
  this.cooldownCost = 200;
}

GatlingTower.prototype = Object.create(BaseTower.prototype);

GatlingTower.prototype.shoot = function() {
    //this.rotation += (Math.random() - 0.5) * 0.1;
    b = new LaserBullet(this.bullet, this.x, this.y, this.rotation, this.damage, 42, this.enemies);
    this.bulletList.push(b);
    this.stage.addChild(b);
}

GatlingTower.prototype.openMenu = function(pos) {
  this.hud.menu = new PopupUpgradeMenu(this.player, this, pos);
  this.hud.menu.createBackground(pos.x - 155, pos.y + 10, 310, 60);
  this.hud.menu.addButton(pos.x - 100, pos.y + 40, 10, 1.5, "damage", "damageCost", "Damage");
  this.hud.menu.addButton(pos.x, pos.y + 40, 10, 1.5, "range", "rangeCost", "Range");
  this.hud.menu.addButton(pos.x + 100, pos.y + 40, -1, 1.5, "cooldown", "cooldownCost", "Firerate");
  this.stage.addChild(this.hud.menu.getContainer());
}
