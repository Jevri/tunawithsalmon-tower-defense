var IceTower = function(x, y, enemies, bulletList, stg, player, menu, background) {
  // Calls base usng the textures for the tower.
  BaseTower.call(this, PIXI.loader.resources["./resources/images/IceTower.png"].texture, PIXI.loader.resources["./resources/images/IceBullet.png"].texture, x, y, enemies, bulletList, stg, player, menu, background);

  // Sets tower variables.
  this.damage = 4;
  this.range = 200;
  this.cooldown = 20;
  this.radius = 50;
}

IceTower.prototype = Object.create(BaseTower.prototype);

IceTower.prototype.shoot = function() {
  // shoot stright
  b = new IceBullet(this.bullet, this.x, this.y, this.rotation, this.damage, 10, this.radius, this.enemies);
  this.bulletList.push(b);
  this.stage.addChild(b);
}
