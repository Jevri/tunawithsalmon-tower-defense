var RocketTower = function(x, y, enemies, bulletList, stg, player, menu, background) {
  // Calls base usng the textures for the tower.
  BaseTower.call(
    this,
    PIXI.loader.resources["./resources/images/RocketTower.png"].texture,
    PIXI.loader.resources["./resources/images/RocketBullet.png"].texture,
    x, y, enemies, bulletList, stg, player, menu, background);

  // Sets tower variables.
  this.damage = 50;
  this.range = 600;
  this.cooldown = 300;
  this.radius = 50;
  // Makes it shoot 3 burst with 3 rockets in each burst
  this.burstFire = 0;
  // Times shot before Cooldown is tiggert
  this.timesShot = 0;
  // Distance between each rocket
  this.shootTimer = 8;
  // Distance between each burstfire
  this.pauseTimer = 15;
  this.pauseTimerCounter = 0;
  this.shootTimerCounter = 0;

  //Prices for each upgrade
  this.damageCost = 100;
  this.rangeCost = 100;
  this.cooldownCost = 100;
  this.pauseTimerCost = 100;
  this.shootTimerCost = 100;

}

RocketTower.prototype = Object.create(BaseTower.prototype);

RocketTower.prototype.shoot = function() {
  var rot = this.rotation + Math.random();
  b = new RocketBullet(this.bullet, this.x + Math.cos(rot) / 20, this.y + Math.sin(rot) * 20, this.rotation, this.damage, 42, this.radius, this.enemies);
  this.bulletList.push(b);
  this.stage.addChild(b);
}

RocketTower.prototype.tryShoot = function() {
  if (this.cooldownTimer <= 0) {
    if (this.pauseTimerCounter <= 0) {
      if (this.shootTimerCounter <= 0) {
        if (this.burstFire < 3) {
          if (this.timesShot < 9) {
            this.shoot();
            this.timesShot++;
            //Afstand mellem de 3 hurtige skud
            this.shootTimerCounter = this.shootTimer;
            this.burstFire++;
          } else {
            this.cooldownTimer = this.cooldown;
            this.timesShot = 0;
          }
        } else {
          //pause
          this.pauseTimerCounter = this.pauseTimer;
          this.burstFire = 0;
        }
      }
    }
  }
  this.shootTimerCounter--;
  this.pauseTimerCounter--;
}

RocketTower.prototype.openMenu = function(pos) {
  this.hud.menu = new PopupUpgradeMenu(this.player, this, pos);
  this.hud.menu.createBackground(pos.x - 255, pos.y + 10, 510, 60);
  this.hud.menu.addButton(pos.x - 100, pos.y + 40, 10, 1.5, "damage", "damageCost", "Damage");
  this.hud.menu.addButton(pos.x, pos.y + 40, 10, 1.5, "range", "rangeCost", "Range");
  this.hud.menu.addButton(pos.x + 100, pos.y + 40, -10, 1.5, "cooldown", "cooldownCost", "ReloadT");
  this.hud.menu.addButton(pos.x - 200, pos.y + 40, -1, 1.5, "pauseTimer", "pauseTimerCost", "PauseT");
  this.hud.menu.addButton(pos.x + 200, pos.y + 40, -0.5, 1.5, "shootTimer", "shootTimerCost", "Firerate");
  this.stage.addChild(this.hud.menu.getContainer());
}
