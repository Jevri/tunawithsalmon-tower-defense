var ShotgunTower = function(x, y, enemies, bulletList, stg, player, menu, background){
  // Calls base usng the textures for the tower.
  BaseTower.call(
    this,
    PIXI.loader.resources["./resources/images/ShotgunTower.png"].texture,
    PIXI.loader.resources["./resources/images/ArmyBullet.png"].texture,
    x, y, enemies, bulletList, stg, player, menu, background);

  // Sets tower variables.
  this.damage = 20;
  this.range = 200;
  this.cooldown = 20;
  this.spread = 1.5;
  this.amountOfbullets = 5;

  this.damageCost = 60;
  this.rangeCost = 60;
  this.cooldownCost = 60;
  this.spreadCost = 60;
  this.amountOfbulletsCost = 60;
}
ShotgunTower.prototype = Object.create(BaseTower.prototype);

ShotgunTower.prototype.shoot = function() {
  for (var i = 0; i < this.amountOfbullets; i++) {
    b = new baseBullet(this.bullet, this.x, this.y,
      this.rotation + (Math.random() - 0.5) * this.spread,
      this.damage, 20, this.enemies);
    this.bulletList.push(b);
    this.stage.addChild(b);
  }
}

ShotgunTower.prototype.openMenu = function(pos) {
  this.hud.menu = new PopupUpgradeMenu(this.player, this, pos);
  this.hud.menu.createBackground(pos.x - 255, pos.y + 10, 510, 60);
  this.hud.menu.addButton(pos.x - 100, pos.y + 40, 10, 1.5, "damage", "damageCost", "Damage");
  this.hud.menu.addButton(pos.x, pos.y + 40, 10, 1.5, "range", "rangeCost", "Range");
  this.hud.menu.addButton(pos.x + 100, pos.y + 40, -1, 1.5, "cooldown", "cooldownCost", "Firerate");
  this.hud.menu.addButton(pos.x - 200, pos.y + 40, 1, 1.5, "amountOfbullets", "amountOfbulletsCost", "Bullets");
  this.hud.menu.addButton(pos.x + 200, pos.y + 40, -0.1, 1.5, "spread", "spreadCost", "Spread");
  this.stage.addChild(this.hud.menu.getContainer());
}
