var Wave = function(){
  this.enemiesList = [Demon];
  this.secondList = [BossEnemy, RunnerEnemy, TankEnemy];
  this.bossList = [];
  this.waveList = [];

  this.waveNum = 0;
  this.waveSpawn = 5;

  this.createWave();
}

Wave.prototype.createWave = function() {
  this.waveNum++;

  if (this.waveNum % 10 == 0 && this.secondList.length > 0){
    this.bossList.push(this.secondList[0]);
    this.secondList.splice(0,1);
  }

  if (!(this.waveList.length > 0)){
    for (var i = 0; i < this.waveSpawn; i++) {
      this.waveList[i] = this.getRandomFromlist();
    }
  }
  this.waveSpawn *= 1.1;
};

Wave.prototype.getEnemy = function() {
  if (this.waveList.length > 0) {
    var reEnemy = this.waveList[0];
    this.waveList.splice(0, 1);
    return reEnemy;
  } else return null;
};

Wave.prototype.getRandomFromlist = function () {
  if (this.bossList.length <= 0 || Math.random() > this.waveNum * 0.005){
    return this.enemiesList[Math.floor(Math.random() * this.enemiesList.length)];
  }
  else {
    return this.bossList[Math.floor(Math.random() * this.bossList.length)];
  }
};
